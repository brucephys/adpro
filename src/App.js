import React from 'react';
import './App.css';
import _ from 'lodash'
import { Table, Tab, Divider } from 'semantic-ui-react'
import { createStore, combineReducers } from 'redux';
// import data from './data.json'

const initialState = {
  column: null,
  data: [],
  direction: null,
  filterText: {value:'', value2:'',value3:'',value4:'',value5:''},

};

const reducer = combineReducers({
  keywordTable: keywordTableReducer,
  jvlDataWatchList: jvlDataWatchListReducer,
});

function keywordTableReducer(state = initialState, action) {
  switch (action.type) {
    case 'CHANGE_SORT':{
      console.log('fire')
      console.log(state)
      console.log(action)
      if (state.column === action.column) {
        return {
          ...state,
          data: state.data.reverse(),
          direction:
            state.direction === 'ascending' ? 'descending' : 'ascending',
        }
      }
      return {
        column: action.column,
        data: _.sortBy(state.data, [action.column]),
        direction: 'ascending',
        filterText: state.filterText
      }
    }
    case 'Filter_MESSAGE':{
      const string = action.text
      console.log(string)
      console.log('fire')

      return {
        ...state, filterText: string
      }
    }
    case 'ADD_MESSAGE':{
      console.log('hihi')
      console.log(state)
      console.log({
        ...state, data: action.state
      })
      return {
        ...state, data: action.state
      }
    }
    default:{
      return state;
    }
  }
}

function jvlDataWatchListReducer(runIDs=[], action){
  switch (action.type) {
    case 'ADD_WATCHLIST': {
      console.log(runIDs.concat(action.text))
      return runIDs.concat(action.text)
    }
    case 'DELETE_WATCHLIST':{
      console.log('delete')
      console.log([
        runIDs.slice(0, action.id),
        runIDs.slice(
          action.id + 1, runIDs.length
        ),
      ])
      return [
        ...runIDs.slice(0, action.id),
        ...runIDs.slice(
          action.id + 1, runIDs.length
        ),
      ];
    }
    default: {
      return runIDs;
    }
  }
}

const store = createStore(reducer);

class TableExampleSortable extends React.Component{
  componentDidMount() {
    store.subscribe(() => this.forceUpdate());
  }

  handleClick = (key) => {
    store.dispatch({
      type: 'ADD_WATCHLIST',
      text: key,
    });
    console.log(key)
  };

  render(){
    const state = store.getState();
    const keywordTable = state.keywordTable;
    console.log('aaabbb')
    console.log(keywordTable)
    const {column, data, direction, filterText} = keywordTable
    let newData = data
    if (filterText.value){
      newData = newData.filter( (d) => {
        // console.log('hi')
        // console.log(d)
        return (
          (typeof d.key === 'string' && d.key.toLowerCase().includes(filterText.value.toLowerCase())) ||
          (typeof d.keyword === 'string' && d.keyword.toLowerCase().includes(filterText.value.toLowerCase()))
        )
      })
    } 
    if (filterText.value2){
      newData = newData.filter( (d) => {
        return (
          (typeof d.key === 'string' && d.key.toLowerCase().includes(filterText.value2.toLowerCase())) ||
          (typeof d.keyword === 'string' && d.keyword.toLowerCase().includes(filterText.value2.toLowerCase()))
        )
      })
    }
    if (filterText.value3){
      newData = newData.filter( (d) => {
        return (
          (typeof d.key === 'string' && d.key.toLowerCase().includes(filterText.value3.toLowerCase())) ||
          (typeof d.keyword === 'string' && d.keyword.toLowerCase().includes(filterText.value3.toLowerCase()))
        )
      })
    }
    if (filterText.value4){
      newData = newData.filter( (d) => {
        return (
          (typeof d.key === 'string' && d.key.toLowerCase().includes(filterText.value4.toLowerCase())) ||
          (typeof d.keyword === 'string' && d.keyword.toLowerCase().includes(filterText.value4.toLowerCase()))
        )
      })
    }
    if (filterText.value5){
      newData = newData.filter( (d) => {
        return (
          (typeof d.key === 'string' && d.key.toLowerCase().includes(filterText.value5.toLowerCase())) ||
          (typeof d.keyword === 'string' && d.keyword.toLowerCase().includes(filterText.value5.toLowerCase()))
        )
      })
    }
    console.log(newData)
    return (
      <Table sortable celled fixed striped collapsing>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell
              sorted={column === 'i' ? direction : null}
              onClick={() => store.dispatch({ type: 'CHANGE_SORT', column: 'i' })}
              collapsing textAlign="center"
              //width = {1}
            >
              #
            </Table.HeaderCell>
            <Table.HeaderCell
              sorted={column === 'key' ? direction : null}
              onClick={() => store.dispatch({ type: 'CHANGE_SORT', column: 'key' })}
              //width = {1}
  
            >
              Run Number
            </Table.HeaderCell>
            <Table.HeaderCell
              sorted={column === 'keyword' ? direction : null}
              onClick={() => store.dispatch({ type: 'CHANGE_SORT', column: 'keyword' })}
            >
              Keyword
            </Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {newData.map(({ i, key, keyword, JVL}, index) => (
            <Table.Row key={index}>
              <Table.Cell collapsing textAlign="center">{i}</Table.Cell>
              <Table.Cell collapsing onClick={()=>this.handleClick(key)} >{key}</Table.Cell>
              <Table.Cell>{keyword}</Table.Cell>
              {/* <Table.Cell>{gender}</Table.Cell> */}
            </Table.Row>
          ))}
        </Table.Body>
      </Table>
    )
  }
}

// const TableExampleSortable = (tableData, handleClick) => {
//   // console.log('table1')
//   // console.log(tableData)
//   // const searchString = tableData.tableData.filterText
//   // const data = tableData.tableData.data
//   const {column, data, direction, filterText} = tableData.tableData
  
//   // const [state, dispatch] = React.useReducer(reducer, {
//   //   column: null,
//   //   data: sdata,
//   //   direction: null,
//   //   filterText: searchString
//   // })

//   // const { column, data, direction,  } = state

//   let newData = data
//   // console.log('table2')
//   // console.log(tableData.tableData)
//   // console.log(filterText)
//   // console.log(filterText.value)
//   // console.log(newData)
//   if (filterText.value){
//     newData = newData.filter( (d) => {
//       // console.log('hi')
//       // console.log(d)
//       return (
//         (typeof d.key === 'string' && d.key.toLowerCase().includes(filterText.value.toLowerCase())) ||
//         (typeof d.keyword === 'string' && d.keyword.toLowerCase().includes(filterText.value.toLowerCase()))
//       )
//     })
//   } 
//   console.log(filterText.value2)
//   console.log(newData)

//   if (filterText.value2){
//     newData = newData.filter( (d) => {
//       // console.log('hi')
//       // console.log(d)
//       return (
//         (typeof d.key === 'string' && d.key.toLowerCase().includes(filterText.value2.toLowerCase())) ||
//         (typeof d.keyword === 'string' && d.keyword.toLowerCase().includes(filterText.value2.toLowerCase()))
//       )
//     })
//   }
//   console.log(newData)

//   // console.log(data)
//   // console.log(newData)

//   return (
//     <Table sortable celled fixed striped collapsing>
//       <Table.Header>
//         <Table.Row>
//         <Table.HeaderCell
//             sorted={column === 'key' ? direction : null}
//             onClick={() => store.dispatch({ type: 'CHANGE_SORT', column: 'key' })}
//             //width = {1}

//           >
//             Run Number
//           </Table.HeaderCell>
//           <Table.HeaderCell
//             sorted={column === 'keyword' ? direction : null}
//             onClick={() => store.dispatch({ type: 'CHANGE_SORT', column: 'keyword' })}
//           >
//             Keyword
//           </Table.HeaderCell>
//           {/* <Table.HeaderCell
//             sorted={column === 'gender' ? direction : null}
//             onClick={() => store.dispatch({ type: 'CHANGE_SORT', column: 'gender' })}
//           >
//             Gender
//           </Table.HeaderCell> */}
//         </Table.Row>
//       </Table.Header>
//       <Table.Body>
//         {newData.map(({ key, keyword, JVL}, index) => (
//           <Table.Row key={index}>
//             <Table.Cell collapsing onClick={() => handleClick(key)} >{key}</Table.Cell>
//             <Table.Cell>{keyword}</Table.Cell>
//             {/* <Table.Cell>{gender}</Table.Cell> */}
//           </Table.Row>
//         ))}
//       </Table.Body>
//     </Table>
//   )
// }


function ImportData() {
  let fileReader;

  const handleFileRead = (e) => {
      const content = fileReader.result;
      const content2 = JSON.parse(content);
      // console.log(content2)
      store.dispatch({
        type: 'ADD_MESSAGE',
        state: content2,
      });
  };
  
  const handleFileChosen = (file) => {
    fileReader = new FileReader();
    fileReader.onloadend = handleFileRead;
    fileReader.readAsText(file);
  };

  return (
    <div>
      <input
        type='file'
        id='file'
        className='input-file'
        accept='.json'
        onChange={e => handleFileChosen(e.target.files[0])}
      />
    </div>
  );
}

class FilterInput extends React.Component {
  constructor(props) {
    super(props);
    store.subscribe(() => this.forceUpdate());
    this.state = store.getState().keywordTable.filterText;
    // this.state = {value:'', value2:'',value3:'',value4:'',value5:''};
  }


  // componentDidMount() {
  //   store.subscribe(() => this.forceUpdate());
  //   this.setState = store.getState().keywordTable.filterText;
  // }


  onChangeFilter1 = (e) => {
    this.setState({
      ...this.state, 
      value:e.target.value},
    )
    console.log('this.state.value')
    console.log({
      ...this.state, 
      value:e.target.value
    });
    store.dispatch({
      type: 'Filter_MESSAGE',
      text:  {
        ...this.state, 
        value:e.target.value},
    });
  };

  onChangeFilter2 = (e) => {
    this.setState({...this.state, value2:e.target.value})
    console.log('this.state.value')
    console.log(this.state)
    store.dispatch({
      type: 'Filter_MESSAGE',
      text:  {...this.state, value2:e.target.value},
    });
  };

  onChangeFilter3 = (e) => {
    this.setState({...this.state, value3:e.target.value})
    console.log('this.state.value')
    console.log(this.state)
    store.dispatch({
      type: 'Filter_MESSAGE',
      text:  {...this.state, value3:e.target.value},
      // text: {value: this.state.value, value2:e.target.value},
    });
  };

  onChangeFilter4 = (e) => {
    this.setState({...this.state, value4:e.target.value})
    console.log('this.state.value')
    console.log(this.state)
    store.dispatch({
      type: 'Filter_MESSAGE',
      text:  {...this.state, value4:e.target.value},
    });
  };

  onChangeFilter5 = (e) => {
    this.setState({...this.state, value5:e.target.value})
    console.log('this.state.value')
    console.log(this.state)
    store.dispatch({
      type: 'Filter_MESSAGE',
      text:  {...this.state, value5:e.target.value},
    });
  };

  render() {
    // const state = store.getState();
    // const filterText = state.keywordTable.filterText
    console.log('this.state.value1234')
    console.log(this.state)


    return (
      <div>
        <div className='ui input'>
          <input
            onChange={this.onChangeFilter1}
            value={this.state.value}
            type='text'
            placeholder= "Filter 1"
          />
        </div>
        <div className='ui input'>
          <input
            onChange={this.onChangeFilter2}
            value={this.state.value2}
            type='text'
            placeholder= "Filter 2"
          />
        </div>
        <div className='ui input'>
          <input
            onChange={this.onChangeFilter3}
            value={this.state.value3}
            type='text'
            placeholder= "Filter 3"
          />
        </div>
        <div className='ui input'>
          <input
            onChange={this.onChangeFilter4}
            value={this.state.value4}
            type='text'
            placeholder= "Filter 4"
          />
        </div>
        <div className='ui input'>
          <input
            onChange={this.onChangeFilter5}
            value={this.state.value5}
            type='text'
            placeholder= "Filter 5"
          />
        </div>
      </div>
    );
  }
}

class KeywordTab extends React.Component {
  // componentDidMount() {
  //   store.subscribe(() => this.forceUpdate());
  // }

  render(){
    // const state = store.getState();
    // const keywordTable = state.keywordTable;
    // console.log('hihi123')
    // console.log(state)

    return(
      <div>
        {/* <FilterInput filterText= {state.keywordTable.filterText}/> */}
        <FilterInput />
        <TableExampleSortable/> 
        {/* <TableExampleSortable tableData = {keywordTable} />  */}
      </div>

    )
  }
}

class JVLDataTab extends React.Component{
  componentDidMount() {
    store.subscribe(() => this.forceUpdate());
  }

  render(){
    const state = store.getState();
    const jvlData = state.keywordTable.data
    const tables = jvlData.map( t => ({
      key: t.key,
      keyword: t.keyword,
      JVL: t.JVL
    }));
    // console.log(this.props.data);
    return(
      <div >
        <JVLTableInput />

        <Divider hidden />

        <JVLTables tables={tables} />
      </div>
    )
  }
}

// class JVLTableInputSearch extends React.Component {

//   handleDropdownChange = (event, data) => {
//     console.log('dropdown')
//     console.log(event)
//     console.log(data)
//   };

//   render(){
//     // const stateOptions= [
//     //   {text: '1',value: 'kannada'},
//     //   {text: '2', value: 'english'},
//     //   {text: '3',value: 'hindhi'}
//     // ]
//     const state = store.getState();
//     const data = state.keywordTable.data;

//     const stateOptions = _.map(data, (state, index) => ({
//       key: index,
//       text: state.key,
//       value: state.key,
//     }))
    
//     console.log(data)

//     return(
//       <div >
//         <Dropdown
//           placeholder='SampleID'
//           onChange = {this.handleDropdownChange}
//           fluid
//           multiple
//           search
//           selection
//           options={stateOptions}
//         />
//       </div>
//     );
//   }
// }

class JVLTableInput extends React.Component {
  state = {
    value: '',
  };

  onChange = (e) => {
    this.setState({
      value: e.target.value,
    })
  };

  handleSubmit = () => {
    store.dispatch({
      type: 'ADD_WATCHLIST',
      text: this.state.value,
    });
    this.setState({
      value: '',
    });
  };

  render() {
    return (
      <div className='ui input'>
        <input
          onChange={this.onChange}
          value={this.state.value}
          type='text'
        />
        <button
          onClick={this.handleSubmit}
          className='ui primary button'
          type='submit'
        >
          Submit
        </button>
      </div>
    );
  }
}

class JVLTables extends React.Component {
  componentDidMount() {
    store.subscribe(() => this.forceUpdate());
  }

  render() {
    const state = store.getState();
    const jvlDataWatchList = state.jvlDataWatchList;
    const tables = this.props.tables
    const filteredTable = jvlDataWatchList.map ( id =>
      tables.find( m => m.key === id)
    )
    console.log(filteredTable)

    return (        
      // <div>
      //   hello
      // </div>
        <JTables
          tables={filteredTable}
          // onClick={(id) => (
          //   store.dispatch({
          //     type: 'OPEN_THREAD',
          //     id: id,
          //   })
          // )}
        />

    );
  }
}

const JTables = (props) => (
  <div >
    {
      props.tables.map((table, index) => (
        <div
          key={index}
          // className={tab.active ? 'active item' : 'item'}
          // onClick={() => props.onClick(tab.id)}
        >
          {<JTable data = {table}/>}
        </div>
      ))
    }
  </div>
);


const JTable = (props) => {
  console.log(props.data.JVL)
  let newData = props.data.JVL.filter((d)=>{
    return(
      d.SampleID.includes('d4')
    )
  })
  return(
    <Table celled selectable>
      <Table.Header>
          <Table.Row>
            <Table.HeaderCell>SampleID</Table.HeaderCell>
            <Table.HeaderCell>CIE_X</Table.HeaderCell>
            <Table.HeaderCell>CIE_Y</Table.HeaderCell>
            <Table.HeaderCell>Luminance</Table.HeaderCell>
            <Table.HeaderCell>Qe</Table.HeaderCell>
            <Table.HeaderCell>Voltage</Table.HeaderCell>
            <Table.HeaderCell>Wvl_nm</Table.HeaderCell>
          </Table.Row>
      </Table.Header>
      <Table.Body>
        {
          newData.map((data) => (
            <Table.Row key={data.SampleID}>
              <Table.Cell> {data.SampleID} </Table.Cell>
              <Table.Cell>{data.CIE_X}</Table.Cell>
              <Table.Cell> {data.CIE_Y} </Table.Cell>
              <Table.Cell>{data.Luminance}</Table.Cell>
              <Table.Cell> {data.Qe} </Table.Cell>
              <Table.Cell>{data.Voltage}</Table.Cell>
              <Table.Cell>{data.Wvl_nm}</Table.Cell>
            </Table.Row>
          ))
        }
      </Table.Body>
    </Table>
  )
}

class WatchList extends React.Component {
  componentDidMount() {
    store.subscribe(() => this.forceUpdate());
  }

  handleClick = (id) => {
    store.dispatch({
      type: 'DELETE_WATCHLIST',
      id: id,
    });
  };
  render() {
    const state = store.getState();
    const jvlDataWatchList = state.jvlDataWatchList;
    console.log(jvlDataWatchList)
    let watchList
    let watchListTable
    if (jvlDataWatchList.length > 0){
      watchList = jvlDataWatchList.map((runid, index) => (
        <Table.Cell
          // className='comment'
          key={index}
          onClick={() => this.handleClick(index)}
        >
            {runid}
        </Table.Cell>
      ));
      watchListTable = 
        <div className = 'ui segment'>
          <Table collapsing>
            <Table.Body>
              <Table.Row textAlign='center'>
                {watchList}
              </Table.Row>
            </Table.Body>  
          </Table>
        </div>
    }
    return (
      <div>
        {watchListTable}
      </div>
    );
  }
}

class App extends React.Component {
  render() {
    const panes =[
      {
        menuItem: 'Keyword',
        render: () => (
          <Tab.Pane attached={false} >
            <KeywordTab/>
          </Tab.Pane>
        ),
      },
      {
        menuItem: 'JVL Data',
        render: () => (
          <Tab.Pane attached={false} >
            <JVLDataTab/>
          </Tab.Pane>
        ),
      },
    ]
    const TabExamplePointing = () => <Tab menu={{ pointing: true }} panes={panes} />

    return(
      <div className = 'ui segment'>
        <div className = 'ui segment'>
          <ImportData />
        </div>
        <WatchList/>
        <TabExamplePointing/>
      </div>
    );
  }

}


export default App;


// git add .    
// git commit -m "Initial commit"
// git push -u origin master     


// cd existing_folder
// git init
// git remote add origin https://gitlab.com/hlihk1/adpro.git
// git add .
// git commit -m "Initial commit"
// git push -u origin master