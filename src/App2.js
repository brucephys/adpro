import React from 'react';
import './App.css';
import _ from 'lodash'
import { Table, Tab, Divider } from 'semantic-ui-react'
import { createStore, combineReducers } from 'redux';

const initialState = {
  column: null,
  data: [
  //   { runN: 'H-010720-A', key: "HIL: HATCN 100â„« | HTL: HHT44 400â„« | EBL: HTM023 50â„« | EML: HM203:GD785 12% 400â„« | ETL: Liq:NTU66 35% 350â„« | EIL: Liq 10â„« | Cath: Al 1000â„«"},
  //   { runN: 'H-010720-B', key: "HIL: HATCN 100â„« | HTL: HHT44 400â„« | EBL: HTM023 50â„« | EML: HM203:GD582 12% 400â„« | ETL: Liq:NTU66 35% 350â„« | EIL: Liq 10â„« | Cath: Al 1000â„«"},
  ],
  direction: null,
  filterText: '',

};

const reducer = combineReducers({
  keywordTable: keywordTableReducer,
  jvlDataWatchList: jvlDataWatchListReducer,
});

function keywordTableReducer(state = initialState, action) {
  switch (action.type) {
    case 'CHANGE_SORT':{
      console.log('fire')
      console.log(state)
      console.log(action)
      if (state.column === action.column) {
        return {
          ...state,
          data: state.data.reverse(),
          direction:
            state.direction === 'ascending' ? 'descending' : 'ascending',
        }
      }
      return {
        column: action.column,
        data: _.sortBy(state.data, [action.column]),
        direction: 'ascending',
        filterText: state.filterText
      }
    }
    case 'Filter_MESSAGE':{
      const string = action.text

      return {
        ...state, filterText: string
      }
    }
    case 'ADD_MESSAGE':{
      console.log('hihi')
      console.log(state)
      console.log({
        ...state, data: action.state
      })
      return {
        ...state, data: action.state
      }
    }
    default:{
      return state;
    }
  }
}

function jvlDataWatchListReducer(runIDs=[], action){
  switch (action.type) {
    case 'ADD_WATCHLIST': {
      console.log(runIDs.concat(action.text))
      return runIDs.concat(action.text)
    }
    case 'DELETE_WATCHLIST':{
      console.log('delete')
      console.log([
        runIDs.slice(0, action.id),
        runIDs.slice(
          action.id + 1, runIDs.length
        ),
      ])
      return [
        ...runIDs.slice(0, action.id),
        ...runIDs.slice(
          action.id + 1, runIDs.length
        ),
      ];
    }
    default: {
      return runIDs;
    }
  }
}

const store = createStore(reducer);







// const store = createStore(reducer, initialState);


// function reducer(state, action) {
//   switch (action.type) {
//     case 'CHANGE_SORT':{
//       console.log('fire')
//       console.log(state)
//       console.log(action)
//       if (state.column === action.column) {
//         return {
//           ...state,
//           data: state.data.reverse(),
//           direction:
//             state.direction === 'ascending' ? 'descending' : 'ascending',
//         }
//       }
//       return {
//         column: action.column,
//         data: _.sortBy(state.data, [action.column]),
//         direction: 'ascending',
//         filterText: state.filterText
//       }
//     }
//     case 'Filter_MESSAGE':{
//       const string = action.text

//       return {
//         ...state, filterText: string
//       }
//     }
//     case 'ADD_MESSAGE':{
//       console.log('hihi')
//       console.log(state)
//       console.log({
//         ...state, data: action.state
//       })
//       return {
//         ...state, data: action.state
//       }
//     }
//     default:{
//       return state;
//     }
//   }
// }


const TableExampleSortable = (tableData) => {
  console.log('table1')
  console.log(tableData)
  // const searchString = tableData.tableData.filterText
  // const data = tableData.tableData.data
  const {column, data, direction, filterText} = tableData.tableData
  
  // const [state, dispatch] = React.useReducer(reducer, {
  //   column: null,
  //   data: sdata,
  //   direction: null,
  //   filterText: searchString
  // })

  // const { column, data, direction,  } = state

  let newData = []
  if (filterText){
    newData = data.filter( (d) => {
      return (d.runN.toLowerCase().includes(filterText.toLowerCase()) ||
        d.key.toLowerCase().includes(filterText.toLowerCase())
      )
    })
  } else{
    newData = data
  }
  
  console.log(data)
  console.log(newData)

  return (
    <Table sortable celled fixed striped collapsing>
      <Table.Header>
        <Table.Row>
        <Table.HeaderCell
            sorted={column === 'name' ? direction : null}
            onClick={() => store.dispatch({ type: 'CHANGE_SORT', column: 'runN' })}
            //width = {1}

          >
            Run Number
          </Table.HeaderCell>
          <Table.HeaderCell
            sorted={column === 'age' ? direction : null}
            onClick={() => store.dispatch({ type: 'CHANGE_SORT', column: 'key' })}
          >
            Keyword
          </Table.HeaderCell>
          {/* <Table.HeaderCell
            sorted={column === 'gender' ? direction : null}
            onClick={() => store.dispatch({ type: 'CHANGE_SORT', column: 'gender' })}
          >
            Gender
          </Table.HeaderCell> */}
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {newData.map(({ runN, key }) => (
          <Table.Row key={runN}>
            <Table.Cell collapsing>{runN}</Table.Cell>
            <Table.Cell>{key}</Table.Cell>
            {/* <Table.Cell>{gender}</Table.Cell> */}
          </Table.Row>
        ))}
      </Table.Body>
    </Table>
  )
}


function ImportData() {
  let fileReader;

  const handleFileRead = (e) => {
      const content = fileReader.result;
      const content2 = JSON.parse(content);
      // console.log(content2)
      store.dispatch({
        type: 'ADD_MESSAGE',
        state: content2,
      });
  };
  
  const handleFileChosen = (file) => {
    fileReader = new FileReader();
    fileReader.onloadend = handleFileRead;
    fileReader.readAsText(file);
  };

  return (
    <div>
      <input
        type='file'
        id='file'
        className='input-file'
        accept='.json'
        onChange={e => handleFileChosen(e.target.files[0])}
      />
    </div>
  );
}

class FilterInput extends React.Component {
  // constructor(props) {
  //   super(props);
  //   // this.state = {value: '',};
  // }

  state = {
    value: '',
  };

  onChange = (e) => {
    this.setState({
      value: e.target.value,
    })
    console.log('this.state.value')
    console.log(this.state)
    store.dispatch({
      type: 'Filter_MESSAGE',
      text: e.target.value,
    });
  };

  render() {

    return (
      <div>
        <div className='ui input'>
          <input
            onChange={this.onChange}
            value={this.state.value}
            type='text'
            placeholder= "Search"
          />
        </div>
      </div>
    );
  }
}

class Keyword extends React.Component {
  componentDidMount() {
    store.subscribe(() => this.forceUpdate());
  }
  render(){
    const state = store.getState();
    const keywordTable = state.keywordTable;
    return(
      <div>
        <FilterInput />
        <TableExampleSortable tableData = {keywordTable} /> 
      </div>

    )
  }
}

class JVLData extends React.Component{
  render(){
    const tables = this.props.data.map( t => ({
      key: t.key,
      keyword: t.keyword,
      JVL: t.JVL
    }));
    // console.log(this.props.data);
    return(
      <div >
        <JVLTables tables={tables} />
      </div>
    )
  }
}


class JVLTables extends React.Component {
  componentDidMount() {
    store.subscribe(() => this.forceUpdate());
  }

  render() {
    const state = store.getState();
    const jvlDataWatchList = state.jvlDataWatchList;
    const tables = this.props.tables
    const filteredTable = jvlDataWatchList.map ( id =>
      tables.find( m => m.key === id)
    )

    return (        
          <JTables
          tables={filteredTable}
          // tables={filteredTable}
  
          // onClick={(id) => (
          //   store.dispatch({
          //     type: 'OPEN_THREAD',
          //     id: id,
          //   })
          // )}
        />

    );
  }
}

const JTables = (props) => (
  <div >
    {
      props.tables.map((table, index) => (
        <div
          key={index}
          // className={tab.active ? 'active item' : 'item'}
          // onClick={() => props.onClick(tab.id)}
        >
          {<JTable data = {table}/>}
        </div>
      ))
    }
  </div>
);

const JTable = (props) => {
  console.log(props.data.JVL)
  return(
    <Table celled selectable>
      <Table.Header>
          <Table.Row>
            <Table.HeaderCell>SampleID</Table.HeaderCell>
            <Table.HeaderCell>CIE_X</Table.HeaderCell>
            <Table.HeaderCell>CIE_Y</Table.HeaderCell>
            <Table.HeaderCell>Luminance</Table.HeaderCell>
            <Table.HeaderCell>Qe</Table.HeaderCell>
            <Table.HeaderCell>Voltage</Table.HeaderCell>
            <Table.HeaderCell>Wvl_nm</Table.HeaderCell>
          </Table.Row>
      </Table.Header>
      <Table.Body>
        {
          props.data.JVL.map((data) => (
            <Table.Row key={data.SampleID}>
              <Table.Cell> {data.SampleID} </Table.Cell>
              <Table.Cell>{data.CIE_X}</Table.Cell>
              <Table.Cell> {data.CIE_Y} </Table.Cell>
              <Table.Cell>{data.Luminance}</Table.Cell>
              <Table.Cell> {data.Qe} </Table.Cell>
              <Table.Cell>{data.Voltage}</Table.Cell>
              <Table.Cell>{data.Wvl_nm}</Table.Cell>
            </Table.Row>
          ))
        }
      </Table.Body>
    </Table>
  )
}

class WatchList extends React.Component {
  handleClick = (id) => {
    store.dispatch({
      type: 'DELETE_WATCHLIST',
      id: id,
    });
  };
  render() {
    const watchList = this.props.watchList.map((runid, index) => (
      <Table.Cell
        // className='comment'
        key={index}
        onClick={() => this.handleClick(index)}
      >
          {runid}
      </Table.Cell>
    ));
    return (
      <Table collapsing>
        <Table.Body>
          <Table.Row textAlign='center'>
            {watchList}
          </Table.Row>
        </Table.Body>  
      </Table>
    );
  }
}





class App extends React.Component {
  // componentDidMount() {
  //   store.subscribe(() => this.forceUpdate());
  // }

  render() {
    // const state = store.getState();
    // const jvlDataWatchList = state.jvlDataWatchList;
    // // const state = store.getState();
    // const jvlData = state.keywordTable.data

    const panes =[
      {
        menuItem: 'Keyword',
        render: () => (
          <Tab.Pane attached={false}>
            <Keyword/>
          </Tab.Pane>
        ),
      },
      // {
      //   menuItem: 'JVL Data',
      //   render: () => (
      //     <Tab.Pane attached={false}>
      //       <JVLData data = {data}/>
      //     </Tab.Pane>
      //   ),
      // },
    ]
    const TabExamplePointing = () => <Tab menu={{ pointing: true }} panes={panes} />

    // console.log("A")
    // console.log(state)
    return(
      <div className = 'ui segment'>
        <div className = 'ui segment'>
          <ImportData />
        </div>
        {/* <div className = 'ui segment'>
          <WatchList watchList={jvlDataWatchList}/>
        </div> */}
        <TabExamplePointing/>

      </div>
    );
  }

}



// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

export default App;


// git add .    
// git commit -m "Initial commit"
// git push -u origin master   
